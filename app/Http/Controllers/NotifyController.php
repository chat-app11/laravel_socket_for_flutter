<?php

namespace App\Http\Controllers;

use App\Events\notifyEvent;
use Illuminate\Http\Request;

class NotifyController extends Controller
{
    //
    public function index()
    {
        return view('send');
    }

    public function viewAccept()
    {
        return view('accept');
    }

    public function notifyEvent()
    {
        //eksekusi event 
        broadcast(new notifyEvent("hellooo"));
    }
}
